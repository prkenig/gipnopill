import React from 'react';
import {connect} from "react-redux";

export class Detail extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            sort: 'name'
        };

        this.setSortDirection = this.setSortDirection.bind(this);
    }

    setSortDirection(target) {
        this.setState({
            sort: target,
        });
    }

    static compareYear(a, b) {
        if (a.year > b.year) {
            return 1;
        } else if (a.year < b.year) {
            return -1;
        } else {
            return 0;
        }
    }

    static compareName(a, b) {
        const nameA = a.name.toUpperCase();
        const nameB = b.name.toUpperCase();

        if (nameA > nameB) {
            return 1;
        } else if (nameA < nameB) {
            return -1;
        } else {
            return 0;
        }
    }

    getAuthor() {
        let id = parseInt(this.props.match.params.id);
        return this.props.authors.find(author => author.id === id);
    }

    render() {
        const author = this.getAuthor();

        if (!author) {
            return <div>Извините, автор не найден</div>
        }

        const books = author.books;

        books.sort(this.state.sort === 'year' ? Detail.compareYear : Detail.compareName);

        return (<>
            <div className="mb-3">
                <h1>{author.name} / {author.nickname} (#{author.id})</h1>
            </div>
            <div className="mb-1">
                <span>Сортировать по: </span>
                <a href="#!" onClick={() => {this.setSortDirection('year')}}>дате выхода</a>,
                <span> </span>
                <a href="#!" onClick={() => {this.setSortDirection('name')}}>по названию.</a>
            </div>
            <div className="row">
                <div className="col-md-4">
                    <div className="list-group">
                        {
                            books.map((book, index) => (
                                <div key={index} className="list-group-item">{book.name} (Год: {book.year})</div>
                            ))
                        }
                    </div>
                </div>
            </div>
        </>);
    }
}

const mapStateToProps = store => {
    return {
        authors: store.authors.list
    }
};

export default connect(mapStateToProps)(Detail);