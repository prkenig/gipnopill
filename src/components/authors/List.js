import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux'

const List = (props) => {
    const { authors } = props;

    return (
        <div className="row">
            <div className="col-md-4">
                <div className="list-group">
                    {
                        authors.map(a => (
                            <Link key={a.id} to={`/authors/${a.id}`} className="list-group-item list-group-item-action">{a.name} / {a.nickname} (Книг: {a.books.length})</Link>
                        ))
                    }
                </div>
            </div>
        </div>
       )
};


const mapStateToProps = store => {
    return {
        authors: store.authors.list
    }
};

export default connect(mapStateToProps)(List);