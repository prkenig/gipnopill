import React from 'react'
import PropTypes from 'prop-types'

export class AuthorForm extends React.Component {

    constructor(props) {
        super(props);

        this.refForm = React.createRef();

        this.state = {
            name: "",
            nickname: "",
            books:[
                {
                    name: "",
                    year: "",
                }
            ],
            errors: {}
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleInputChangeBook = this.handleInputChangeBook.bind(this);
        this.addBook = this.addBook.bind(this);
        this.validate = this.validate.bind(this);
    }

    handleInputChangeBook(event, index) {
        const target = event.target;
        let value = target.value;
        const name = target.name;

        this.state.books[index][name] = value;

        this.setState({
            books: this.state.books,
        });
    }

    validate() {
        this.state.books.map((book, index) => {
            if(book.year === '') {
                this.state.errors['year_'+index] = 'Не может быть пустым';
            } else if(/^[\d]{4}$/.test(book.year) !== true) {
                this.state.errors['year_'+index] = 'Не верный формат';
            } else {
                delete this.state.errors['year_'+index];
            }

            if(book.name === ''){
                this.state.errors['name_'+index] = 'Не может быть пустым';
            } else {
                delete this.state.errors['name_'+index];
            }
        });

        if(this.state.name === '') {
            this.state.errors.name = 'Не может быть пустым';
        } else {
            delete this.state.errors.name;
        }

        if(this.state.nickname === '') {
            this.state.errors.nickname = 'Не может быть пустым';
        } else {
            delete this.state.errors.nickname;
        }

        this.setState({
            errors: this.state.errors
        });
    }

    handleInputChange(event) {
        const target = event.target;
        let value = target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }

    handleSubmit(event) {
        event.preventDefault();

        this.validate();

        if(Object.keys(this.state.errors).length === 0) {
            delete this.state.errors;
            return this.props.handleSubmit(this.state);
        }
    }

    addBook(event) {
        return this.setState({
            books:  [...this.state.books, {
                name: "",
                year: "",
            }]
        })
    }

    render() {
        return (<form ref={this.refForm} className="needs-validation mb-3">
            <div className="row">
                <div className="col-md-6 mb-3">
                    <label htmlFor="name">Имя</label>
                    <input
                        type="text"
                        className={['form-control', this.state.errors.name ? 'is-invalid' : ''].join(' ')}
                        placeholder=""
                        autoComplete="Off"
                        name="name"
                        onChange={this.handleInputChange}
                        defaultValue={this.state.name}
                    />
                    <div className="invalid-feedback">{this.state.errors.name ? this.state.errors.name : ''}</div>
                </div>
                <div className="col-md-6 mb-3">
                    <label htmlFor="nickname">Псевдоним</label>
                    <input
                        type="text"
                        className={['form-control', this.state.errors.nickname ? 'is-invalid' : ''].join(' ')}
                        placeholder=""
                        name="nickname"
                        onChange={this.handleInputChange}
                        defaultValue={this.state.nickname}
                    />
                    <div className="invalid-feedback">{this.state.errors.nickname ? this.state.errors.nickname : ''}</div>
                </div>
            </div>

            {this.state.books.map((book, index) => {
                return (
                    <div key={index} className="row">
                        <div className="col-md-4 mb-3">
                            <label htmlFor="name">Название книги</label>
                            <input
                                type="text"
                                className={['form-control', this.state.errors['name_'+index] ? 'is-invalid' : ''].join(' ')}
                                placeholder=""
                                autoComplete="Off"
                                name="name"
                                onChange={(event) => {this.handleInputChangeBook(event, index)}}
                                defaultValue={book.name}
                            />
                            <div className="invalid-feedback">{this.state.errors['name_'+index] ? this.state.errors['name_'+index] : ''}</div>
                        </div>
                        <div className="col-md-4 mb-3">
                            <label htmlFor="year">Год</label>
                            <input
                                type="number"
                                className={['form-control', this.state.errors['year_'+index] ? 'is-invalid' : ''].join(' ')}
                                placeholder=""
                                name="year"
                                onChange={(event) => {this.handleInputChangeBook(event, index)}}
                                defaultValue={book.year}
                            />
                            <div className="invalid-feedback">{this.state.errors['year_'+index] ? this.state.errors['year_'+index] : ''}</div>
                        </div>
                    </div>
                )
            })}

            <div className="row">
                <div className="col-md-8 mb-3">
                    <button type="button" onClick={this.addBook} className="btn btn-success btn-lg btn-block">Добавить еще</button>
                </div>
            </div>

            <hr className="mb-4"/>

            <button onClick={this.handleSubmit} className="btn btn-primary btn-lg btn-block" type="submit">Отправить</button>
        </form>);
    }
}

AuthorForm.propTypes = {
    handleSubmit: PropTypes.func
};