import React from 'react';
import List from "./List";
import Detail from "./Detail";
import { Switch, Route } from 'react-router-dom';

const Authors = (props) => {
    return (<Switch>
        <Route exact path='/authors' component={List}/>
        <Route path='/authors/:id([\d]+)' component={Detail}/>
    </Switch>)
};

export default Authors;