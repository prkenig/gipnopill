import React from 'react';
import {AuthorForm} from "./AuthorForm";
import {addAuthor} from "../../actions/authors";
import {connect} from "react-redux";
import { useHistory } from "react-router-dom";

const AuthorAdd = (props) => {

    let history = useHistory();

    const handleSubmit = (value) => {
        props.addAuthor(value);
        history.push('/authors');
    };

    return (<AuthorForm handleSubmit={handleSubmit} />)
};


const mapDispatchToProps = dispatch => {
    return {
        addAuthor: data => dispatch(addAuthor(data))
    }
};

export default connect(()=>{}, mapDispatchToProps)(AuthorAdd);