import React from 'react';
import Home from "../home/Home";
import Authors from "../authors/Authors";
import AuthorAdd from "../authors/AuthorAdd";
import { Switch, Route } from 'react-router-dom';

class Main extends React.Component {

    render() {
        return <main role="main" className="container">
            <Switch>
                <Route exact path='/' component={Home}/>
                <Route path='/authors' component={Authors}/>
                <Route path='/author-add' component={AuthorAdd}/>
            </Switch>
        </main>;
    }

}

export default Main;