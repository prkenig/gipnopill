import React from 'react';
import { Link } from 'react-router-dom';

class Header extends React.Component {

  render() {
    return <nav className="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
      <a className="navbar-brand" href="#">Тестовое задание</a>
      <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span className="navbar-toggler-icon"></span>
      </button>

      <div className="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul className="navbar-nav mr-auto">
          <li><Link className="nav-link" to='/'>Главная</Link></li>
          <li><Link className="nav-link" to='/authors'>Авторы</Link></li>
          <li><Link className="nav-link" to='/author-add'>Добавить автора</Link></li>
        </ul>
      </div>
    </nav>;
  }
}

export default Header;