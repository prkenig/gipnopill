import {createStore} from 'redux';
import rootReducer from '../reducers/';
import {loadState} from "./localStorage";

const persistedState = loadState();

export default function configureStore() {
    return createStore(rootReducer, persistedState);
}