import Header from './components/layouts/Header';
import Main from './components/layouts/Main';

function App() {
  return (
      <>
          <Header />
          <Main />
      </>
  );
}

export default App;
