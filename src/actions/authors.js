export const ADD_AUTHOR = 'ADD_AUTHOR';

export const addAuthor = (data) => {
    return {
        type: ADD_AUTHOR,
        data: data
    }
};