import { ADD_AUTHOR } from '../actions/authors'

const initialState = {
    list: [
        {
            id: 1,
            name: "Иванов",
            nickname: "ivanov",
            books: []
        },
    ]
};

export default function authors(state = initialState, action) {
    switch(action.type) {
        case ADD_AUTHOR: {
            const count = state.list.length;
            action.data.id = count+1;
            return {
                ...state,
                list: [...state.list, action.data]
            }
        }

        default: return state;
    }
}