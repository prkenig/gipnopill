import { combineReducers } from 'redux'
import authors from './authors';

const rootReducer = combineReducers({
    authors: authors,
});

export default rootReducer;